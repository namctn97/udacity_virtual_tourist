//
//  Flickr Client.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 26/09/2022.
//

import Foundation
import MapKit
import CoreData

class FlickrClient {
    struct Auth {
        static let methodSearch = "flickr.photos.search"
        static let apiKey = "d8ac156a8f8d14841ce3585cbb260c88"
        static var lat = ""
        static var lon = ""
        
    }
    
    enum Endpoint {
        static let base = "https://api.flickr.com/services/rest"
        case getPhoTo( Int)
        var stringValue : String {
            switch self {
            case .getPhoTo(let page):
                return Endpoint.base + "?api_key=" + Auth.apiKey + "&format=json&nojsoncallback=1&radius=1.6&radius_units=km&method="+Auth.methodSearch+"&lat="+Auth.lat+"&lon="+Auth.lon+"&page=" + "\(page)" + "&extras=url_m"
            }
        }
        var url: URL {
            return URL(string: stringValue)!
        }
    }
    class func getPhotosForLocation(page:Int,lat : Double , lon : Double,  completion: @escaping (Photos? , Error?) -> Void) {
        Auth.lat = "\(lat)"
        Auth.lon = "\(lon)"
        var request = URLRequest(url: Endpoint.getPhoTo(page).url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, err in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, err)
                }
                return
            }
            do {
                
                let decoder = JSONDecoder()
                //   print(String(data: data, encoding: .utf8)!)
                let responseObject = try decoder.decode(GetPhotoResponse.self, from: data)
                let dataResponse = responseObject.photos
                DispatchQueue.main.async {
                    completion(dataResponse, nil)
                }
            } catch {
                
                DispatchQueue.main.async {
                    completion(nil , error)
                }
                
            }
            
            
            
        }
        task.resume()
        
    }
    
    
    class func downloadImage(url : String , completion: @escaping (PhotoCoreData? , Error?) -> Void , dataConTroller : DataController) {
        let task = URLSession.shared.dataTask(with: URLRequest(url: URL(string: url)!), completionHandler: {(data, response, error) -> Void in
            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                    
                }
                return
            }
            
            DispatchQueue.main.async {
                let newData = PhotoCoreData(context: dataConTroller.viewContext)
                newData.data = data
                completion(newData , nil)
            }
            
        }
        )
        
        task.resume()
    }
    
}
