//
//  DialogGlobal.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 29/09/2022.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlertNotification(message: String,title : String) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        show(alertVC, sender: nil)
    }
}
