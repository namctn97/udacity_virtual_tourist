//
//  PhotoCollectionViewCell.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 29/09/2022.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    func setData(image: UIImage) {
           imageView.image = image
       }
}
