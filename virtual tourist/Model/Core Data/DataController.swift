//
//  DataController.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 28/09/2022.
//

import Foundation
import CoreData

class DataController {
    let persistentController:NSPersistentContainer
    
    var viewContext : NSManagedObjectContext{
        return persistentController.viewContext
    }
    init(modelName:String) {
        persistentController = NSPersistentContainer(name: modelName)
    }
    
    func load(completion :(()-> Void)? = nil){
        persistentController.loadPersistentStores { storeDesciption, err in
            guard err == nil else {
                fatalError(err!.localizedDescription)
                
            }
            self.autoSaveViewContext(interval: 3)
            completion?()
        }
    }
}

extension DataController {
    func autoSaveViewContext(interval:TimeInterval = 30){
        guard interval > 0 else {
            print("cannot set negative autosave interval")
            return
        }
        
        if viewContext.hasChanges {
            try? viewContext.save()
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+interval) {
            self.autoSaveViewContext(interval: interval)
        }
    }
}
