//
//  ViewController.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 26/09/2022.
//

import UIKit
import MapKit
import CoreData

class TravelLocationsMapController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    // Mark : outlet
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    // Mark : property
    let locationManager = CLLocationManager()
    
    var dataController : DataController!
    
    var anotations : [Annotation] = []
    
    var annotation : MKAnnotation!
    var annotationCoreData : Annotation!
    
    
    // Mark : life cricle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegateMapView()
        addEvenLongPressInMapView()
        setupFetchResultController()
        
    }
    fileprivate func setupFetchResultController() {
        let fetchRequest : NSFetchRequest<Annotation> = Annotation.fetchRequest()
    
        fetchRequest.sortDescriptors = []
        
        if let result = try? dataController.viewContext.fetch(fetchRequest){
            anotations = result
            
        }
        setUpDataMap()
    }
    private func setUpDataMap() {
        for location in anotations {
            let lat = CLLocationDegrees(location.lantitue)
            let long = CLLocationDegrees(location.lontitue )
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = "\(location.title ?? "") "
            annotation.subtitle = location.subtile
            DispatchQueue.main.async {
                self.mapView.addAnnotation(annotation)
                
            }
        }
    }
    
    fileprivate func delegateMapView() {
        self.mapView.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        self.mapView.showsUserLocation = true
        self.mapView.setUserTrackingMode(.follow, animated: true)
        self.mapView.isPitchEnabled = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.locationManager.stopUpdatingLocation()
            render(location)
            
        }
    }
    func render(_ location : CLLocation){
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
    }
    
    func addEvenLongPressInMapView(){
        let gestureLongPress = UILongPressGestureRecognizer(target: self,action:#selector(self.handleLongPress))
        gestureLongPress.delegate = self
        gestureLongPress.minimumPressDuration = 1
        gestureLongPress.delaysTouchesBegan = true
        self.mapView.addGestureRecognizer(gestureLongPress)
    }
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizer.State.ended {
            return
        }
        else if gestureRecognizer.state != UIGestureRecognizer.State.began {
            
            let touchPoint = gestureRecognizer.location(in: self.mapView)
            let coordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { (data, error) in
                guard let data = data else {
                    self.showAlertNotification(message: error?.localizedDescription ?? "", title: "Create location fail")
                    return
                }
                self.createAnotation(placemark:data[0])
            }
        }
    }
    
    private func  createAnotation(placemark: CLPlacemark){
        let coordinate = CLLocationCoordinate2D(latitude: (placemark.location?.coordinate.latitude)!, longitude: (placemark.location?.coordinate.longitude)!)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = placemark.name ?? "Unknown Place"
        annotation.subtitle = placemark.country ?? ""
       
        DispatchQueue.main.async {
            self.mapView.addAnnotation(annotation)
            
        }
        
        for anotation in anotations {
            if (anotation.lantitue == (placemark.location?.coordinate.latitude)! && anotation.lontitue == placemark.location?.coordinate.longitude ?? 0.0){
                return
            }
        }
        
        addAnnotationInCoreData(param: annotation)
    }
    
    private func addAnnotationInCoreData(param :MKPointAnnotation){
        print("addAnnotationInCoreData")
        let annotation = Annotation(context: dataController.viewContext)
        annotation.title = param.title
        
        annotation.subtile = param.subtitle
        annotation.lontitue = Double(param.coordinate.longitude)
        annotation.lantitue = Double(param.coordinate.latitude)
        annotation.createDate = Date()
        try? dataController.viewContext.save()
        annotationCoreData = annotation
       
    }
}

extension TravelLocationsMapController  : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.animatesDrop = true
            pinView?.image = UIImage(named: "icon_pin")
            pinView?.canShowCallout = true
            pinView?.isDraggable = true
            let rightButton: AnyObject! = UIButton(type: UIButton.ButtonType.detailDisclosure)
            pinView?.rightCalloutAccessoryView = rightButton as? UIView
        }
        else {
            pinView?.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        print(#function)
        if control == view.rightCalloutAccessoryView {
            
            DispatchQueue.main.async { [self] in
                if let photoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotoAlbumViewController") as? PhotoAlbumViewController {
                    photoVC.annotation = view.annotation
                    photoVC.dataController = self.dataController
                    for anotation in anotations {
                        if (anotation.lantitue == view.annotation?.coordinate.latitude && anotation.lontitue == view.annotation?.coordinate.longitude){
                            photoVC.annotationCoreData = anotation
                        }
                    }
                    
                    
                    if (annotation == nil){
                        self.setUpAnnomation(param: view.annotation!)
                    }
                    
                    photoVC.annotationCoreData = self.annotationCoreData
                    if let navigator = self.navigationController {
                        navigator.pushViewController(photoVC, animated: true)
                    }
                }
                
            }
            
        }
    }
    func setUpAnnomation(param : MKAnnotation){
        let annotation = Annotation(context: dataController.viewContext)
        annotation.title = param.title ?? "No Data"
        annotation.subtile = param.subtitle ?? "No Data" 
        annotation.lontitue = Double(param.coordinate.longitude)
        annotation.lantitue = Double(param.coordinate.latitude)
        annotationCoreData = annotation
    }
}




