//
//   PhotoAlbumViewController.swift
//  virtual tourist
//
//  Created by NamCT.FWA on 26/09/2022.
//

import Foundation
import UIKit
import MapKit
import CoreData

class PhotoAlbumViewController : UIViewController,UICollectionViewDelegate,UICollectionViewDataSource{
    
    // Mark outlet
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var colectionView: UICollectionView!
    
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    

    
    
    
    @IBOutlet weak var newCollectionBtn: UIButton!
    
    
    // Mark : poperty
    
    var annotation : MKAnnotation!
    
    var listUrlImage: [String]  = []
    
    var page : Int = 0
    
    var annotationCoreData : Annotation!
    
    var listDataImage : [PhotoCoreData] = []
    
    var numberLimit0fCollection = 20
    
    var dataController : DataController!
    
    
   

    // Mark action
    
    @IBAction func clickButtonNewColection(_ sender: Any) {
        listDataImage.removeAll()
        page += 1
        setUpColectionView()
    }
    // Mark : life circle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.addAnnotation(annotation)
        mapView.centerCoordinate = annotation.coordinate
        setupFetchResultController()
        
    }
    fileprivate func setUpColectionView() {
        let lat = annotation.coordinate.latitude
        let lon = annotation.coordinate.longitude
        colectionView.allowsMultipleSelection = true
        indicator.startAnimating()
        FlickrClient.getPhotosForLocation(page: page, lat: lat, lon: lon,completion: handleErrorPhoto(datas:error:))
    }
    fileprivate func setupFetchResultController() {
        let fetchRequest : NSFetchRequest<Annotation> = Annotation.fetchRequest()
        let predicate = NSPredicate(format: "photos == %@", annotationCoreData)
        fetchRequest.predicate = predicate
      
        fetchRequest.sortDescriptors = []
        
        listDataImage = annotationCoreData.photos as! [PhotoCoreData]
        
       
        
        
        if(listDataImage.isEmpty){
           
            setUpColectionView()
        }
      
    }
    
    
    func handleErrorPhoto(datas : Photos? , error : Error? ){
        if (datas != nil){
            
            guard let photos = datas?.photo else{
                self.showAlertNotification(message: error?.localizedDescription ?? "", title: "Search loaction Error")
                return
            }
            
            for photo in photos {
                let linkUrl = photo.urlM
                listUrlImage.append(linkUrl)
            }
         
            for index in 0...numberLimit0fCollection {
                    FlickrClient.downloadImage(url: listUrlImage[index], completion: self.handleImageToDownload(data:error:),dataConTroller: dataController)
                if (index == numberLimit0fCollection){
                    indicator.stopAnimating()
                }
            }
             
        }else {
            indicator.stopAnimating()
            self.showAlertNotification(message: error?.localizedDescription ?? "", title: "Search loaction Error")
        }
    }
    
    func handleImageToDownload(data : PhotoCoreData? , error : Error?){
        if(error == nil){
            listDataImage.append(data!)
            colectionView.reloadData()
            saveImageInCoreData(data: data!)
            
        }else {
            indicator.stopAnimating()
            self.showAlertNotification(message: error?.localizedDescription ?? "", title: "Load image fail ")
        }
    }
    
    func saveImageInCoreData(data:PhotoCoreData){
        let image = PhotoCoreData(context: dataController.viewContext)
        image.data = data.data
        try? dataController.viewContext.save()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listDataImage.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColectionImageCell", for: indexPath) as! PhotoCollectionViewCell
        cell.imageView.image = UIImage(data: listDataImage[indexPath.row].data! )
        return cell
    }
    override func viewDidDisappear(_ animated: Bool) {
        listDataImage.removeAll()
    }
}
